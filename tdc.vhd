LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_arith.ALL;
USE ieee.math_real.ALL;

-- namiastka przetwornika TDC. Układ ma na celu odmierzać ilość impulsów clk od pojawienia się narastającego zboacza
-- start do narastającego. Stop powoduje pojawienie się licznika na wyjściu.

ENTITY tdc IS
	generic(constant N: integer := 8);
 	port (
		start 		:in bit;
		stop			:in bit;
		rst			:in bit;
	 	clk			:in  bit;                    
		tout		:out std_logic_vector (N-1 downto 0)
  	);
END tdc;

ARCHITECTURE behaviour OF tdc IS
signal counter: unsigned(n-1 downto 0):=(others => '0');
signal tmp: unsigned(n-1 downto 0):=(others => '0');
BEGIN
time_counting_process: 
	process(clk,rst)
	begin
		if (rst='1') then
			counter<=(others => '0');
		elsif (clk'event and clk='1') then
			counter<=counter+1;
		end if;
	end process;
stopping_process:
	process(stop)
	begin
		if(stop'event and stop='1') then
			tmp<=counter;
		end if;
	end process;
	tout<=std_logic_vector(tmp);
END behaviour;
