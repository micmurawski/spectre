library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
---układ wyzwalania stałej czasowej na podstawie amplitudy w pliku jest zaimplementowany komparator (lpm_compare0)
---ktory wyzwala układ time_const

entity amp_time_const is
port(
			clk,rst: in STD_LOGIC;
			period: in unsigned(7 downto 0); --okres zbierania próbek po wyzwoleniu amplitudą
			input_lvl		: IN STD_LOGIC_VECTOR (7 DOWNTO 0); --amplituda badana
			trigger_lvl		: IN STD_LOGIC_VECTOR (7 DOWNTO 0); --amplituda wyzwolenia
			output: out bit
			);
end amp_time_const;

architecture arch1 of amp_time_const is
signal aleb: STD_LOGIC;

component lpm_compare0 
	PORT
	(
		dataa		: IN STD_LOGIC_VECTOR (7 DOWNTO 0);
		datab		: IN STD_LOGIC_VECTOR (7 DOWNTO 0);
		aleb		: OUT STD_LOGIC 
	);
end component;

component time_const is
	generic(
			n: integer:=8
			);
	port(
			clk,req,rst: in STD_LOGIC;
			period: in unsigned(n-1 downto 0);
			output: out bit
			);
end component;

begin
lpm_compare1: lpm_compare0 PORT MAP(input_lvl,trigger_lvl,aleb);
time_const1: time_const PORT MAP(clk,aleb,rst,period,output);	
end arch1;
			
		
