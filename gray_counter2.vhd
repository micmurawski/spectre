library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
 
entity gray_counter2 is
    port (
        cout   :out std_logic_vector (7 downto 0); -- Output of the counter
        clk    :in  std_logic;                     -- Input clock
        reset  :in  std_logic                      -- Input reset
     );
 end entity;
 
 architecture rtl of gray_counter2 is
     signal count :std_logic_vector (7 downto 0);
	  signal gray :std_logic_vector(7 downto 0);
 begin
     process (clk, reset) begin
         if (reset = '1') then
             count <= (others=>'0');
         elsif (rising_edge(clk)) then
                 count <= count + 1;
        end if;
     end process;
	  
     gray <= (count(7) & 
             (count(7) xor count(6)) & 
             (count(6) xor count(5)) & 
             (count(5) xor count(4)) & 
				 (count(4) xor count(3)) & 
             (count(3) xor count(2)) & 
             (count(2) xor count(1)) & 
             (count(1) xor count(0)) );
				  

		cout(0) <= gray(7) and gray(6) and gray(5) and not(gray(4)) and not(gray(3)) and not(gray(2)) and not(gray(1)) and not(gray(0));
		cout(1) <= (gray(7) and gray(6) and gray(5) and not(gray(4)) and not(gray(3)) and not(gray(2))) and not(gray(1));
		cout(2) <= gray(7) and gray(6) and gray(5) and not(gray(4)) and not(gray(3)) and not(gray(2));
		cout(3) <= gray(7) and gray(6) and gray(5) and not(gray(4)) and not(gray(3));
		cout(4) <= gray(7) and gray(6) and gray(5) and not(gray(4));
		cout(5) <= gray(7) and gray(6) and gray(5);
		cout(6) <= gray(7) and gray(6);
		cout(7) <= gray(6);

end architecture;