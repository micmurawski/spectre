library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--- licznik stałej czasowej
--- układ wystawia stan wysoki na wyjsciu output na zdarzenie req przez zadaną ilość (period) cylki zegara clk.

entity time_const is
	generic(
			n: integer:=6
			);
	port(
			clk,req,rst: in STD_LOGIC;
			period: in unsigned(n-1 downto 0);
			output: out bit
			);
end time_const;

architecture arch1 of time_const is

type stany is (count,idle);
signal state,nxt_state:stany;
signal counter: unsigned(n-1 downto 0):=(others => '0');

begin

	process(clk,req,rst)
	begin
		if (rst='1') then
			output<='0';
			counter<=(others => '0');
		elsif (rising_edge(clk)) then
			if(counter = period) then
				output<='0';
				counter<=(others => '0');
			else
			output<='1';
			counter<=counter+1;
			end if;
		end if;
	end process;
	
end arch1;
			
		
