library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity fft_usb is
port(
en_send : in std_logic;
clk : in std_logic;
TxD : out std_logic;
ready : out std_logic
);
end fft_usb;


architecture arch of fft_usb is

type complex is record
Re : signed(7 downto 0);
Im : signed(7 downto 0);
end record complex;

type buff is array (0 to 15) of complex;

type state is (idle, prep, send, eof);

component pll_uart is
	PORT
	(
		inclk0		: IN STD_LOGIC  := '0';
		c0		: OUT STD_LOGIC 
	);
end component;


signal current_state, next_state : state := idle;
signal part : std_logic := '0';
signal clk_uart : std_logic;
signal flag : std_logic := '0';
signal rdy : std_logic := '0';
signal counter : unsigned(13 downto 0) := (others => '0');
signal output : std_logic := '1';
signal frame : std_logic_vector(9 downto 0) := "0000000001";
signal i : integer := 9;
signal n : integer := 0;
signal fft_out : buff := (others =>("01010011", "00111010"));
signal marker : std_logic_vector(0 to 9) := "0001001001";


begin

U1: pll_uart port map (inclk0 => clk, c0 => clk_uart);


process(clk_uart, current_state)
begin

if(current_state = idle) then
	if(en_send = '1') then
		next_state <= prep;
		rdy <= '0';
	else
		next_state <= idle;
	end if;
	
elsif(current_state = prep) then
	
	if(n < 16) then
		rdy <= '0';
		if(part = '0') then
			frame <= '0' & std_logic_vector(fft_out(n).Re(7 downto 0)) & '0';
			part <= '1';
		elsif(part = '1') then
			frame <= '1' & std_logic_vector(fft_out(n).Im(7 downto 0)) & '0';
			part <= '0';
			n <= n+1;
		end if;
		next_state <= send;
	else
		n <= 0;
		next_state <= eof;
	end if;

elsif(current_state = send) then

	if(flag = '0') then
		counter <= counter-1;
		output <= '1';
		next_state <= send;
		if(counter = "00000000000000") then
			flag <= '1';
		end if;
		
	elsif(flag = '1') then
		if(i>-1) then
			output <= frame(i);
			i <= i-1;
			next_state <= send;
		else
			i<=9;
			flag <= '0';
			output <= '1';
			next_state <= prep;
		end if;
	end if;
	
elsif(current_state = eof) then

	if(flag = '0') then
		counter <= counter-1;
		output <= '1';
		next_state <= eof;
		if(counter = "00000000000000") then
			flag <= '1';
		end if;
		
	elsif(flag = '1') then
		if(i<10) then
			output <= marker(i);
			i <= i+1;
			next_state <= eof;
		else
			i<=0;
			flag <= '0';
			output <= '1';
			next_state <= idle;
			rdy <= '1';
		end if;
	end if;
	
end if;
	
current_state <= next_state;

end process;

TxD <= output;
ready <= rdy;
		

end architecture;

